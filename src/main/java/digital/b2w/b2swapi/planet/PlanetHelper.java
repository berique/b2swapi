package digital.b2w.b2swapi.planet;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class PlanetHelper {

    private PlanetRepository planetRepository;

    private static final String PLANET_SOURCE = "https://swapi.co/api/planets/?format=json";

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    protected static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet(url);
        CloseableHttpResponse response = httpClient.execute(request);
        try {

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String jsonText = readAll(rd);
            return new JSONObject(jsonText);
        } finally {
            response.close();
        }
    }

    public List<Planet> obtainAllPlanets() throws IOException {
        final List<Planet> planets = new ArrayList<>();

        AtomicLong id = new AtomicLong(1L);

        String next = obtainPlanet(planets, id, PLANET_SOURCE);

        while (next != null) {
            String myNext = obtainPlanet(planets, id, next);
            next = myNext;
        }

        return planets;
    }

    public String obtainPlanet(List<Planet> planets, AtomicLong id, String url) throws IOException {
        JSONObject object = readJsonFromUrl(url);

        for (Object json : object.getJSONArray("results")) {
            JSONObject jsonObject = ((JSONObject) json);
            Planet planet = createPlanet(id, jsonObject);
            planets.add(planet);
        }

        try {
            return object.getString("next");
        } catch (org.json.JSONException e) {
            return null;
        }
    }

    private Planet createPlanet(AtomicLong id, JSONObject jsonObject) {
        Planet planet = new Planet();
        planet.setId(id.getAndIncrement());
        planet.setName(jsonObject.getString("name"));
        planet.setClimate(jsonObject.getString("climate"));
        planet.setTerrain(jsonObject.getString("terrain"));
        planet.setApparitions(jsonObject.getJSONArray("films").length());
        return planet;
    }
}
