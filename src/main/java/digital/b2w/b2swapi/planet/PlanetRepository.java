package digital.b2w.b2swapi.planet;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "planet", path = "planet")
public interface PlanetRepository extends MongoRepository<Planet, Double> {
    @Override
    Iterable<Planet> findAllById(Iterable<Double> iterable);

    @Override
    Optional<Planet> findById(Double aDouble);

    Iterable<Planet> findByName(@Param("name") String name);
}
