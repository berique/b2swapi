package digital.b2w.b2swapi.planet;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class Planet {

    @Id
    private Long id;

    private String name;

    private String climate;

    private String terrain;

    private Integer apparitions;

}
