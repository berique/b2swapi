package digital.b2w.b2swapi;

import digital.b2w.b2swapi.planet.PlanetHelper;
import digital.b2w.b2swapi.planet.PlanetRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Log
@SpringBootApplication
public class Main {
    @Autowired
    private PlanetRepository planetRepository;

    @Autowired
    private PlanetHelper planetHelper;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

}
