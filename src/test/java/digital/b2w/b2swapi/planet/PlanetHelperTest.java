package digital.b2w.b2swapi.planet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlanetHelperTest {

    @Autowired
    private PlanetHelper planetHelper;

    @Autowired
    private PlanetRepository planetRepository;

    @Test
    public void obtainPlanets() throws Exception {
        List<Planet> planets = planetHelper.obtainAllPlanets();
        assertThat(planets.size(), is(61));
        final Planet tatooine = planets.iterator().next();
        assertThat(tatooine.getId(), is(1L));
        assertThat(tatooine.getName(), is("Alderaan"));
        assertThat(tatooine.getClimate(), is("temperate"));
        assertThat(tatooine.getTerrain(), is("grasslands, mountains"));
        assertThat(tatooine.getApparitions(), is(2));
    }

    @Test
    public void addMongo() throws IOException {
        assertThat(this.planetRepository.count(), is(0L));
        List<Planet> planets = this.planetHelper.obtainAllPlanets();
        this.planetRepository.insert(planets);
        assertThat(this.planetRepository.count(), is(61L));


    }

}