package digital.b2w.b2swapi.planet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PlanetClientTest {

    public static final long ID = 999L;

    @Autowired
    private PlanetRepository planetRepository;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void createPlanet() throws Exception {

        Planet planet = new Planet();

        planet.setId(ID);
        planet.setName("Tatooooo");
        planet.setTerrain("arid");
        planet.setTerrain("desert");
        planet.setApparitions(1);

        ResponseEntity<Planet> result = this.restTemplate.postForEntity(getBaseUrl(),
                planet, Planet.class);

        assertThat(result.getStatusCode(), is(HttpStatus.CREATED));
        assertThat(result.getBody().getName(), is(planet.getName()));
        assertThat(result.getBody().getTerrain(), is(planet.getTerrain()));
        assertThat(result.getBody().getApparitions(), is(planet.getApparitions()));
        assertThat(result.getBody().getClimate(), is(planet.getClimate()));

        assertThat(getCount(), is(1));
    }

    @Test
    public void updatePlanet() throws Exception {

        createPlanet();

        Planet planet = new Planet();

        planet.setId(ID);
        planet.setName("Tatooooo");
        planet.setTerrain("arid");
        planet.setTerrain("desert");
        planet.setApparitions(1);

        final URL url = new URL(getBaseUrl() + planet.getId() + "/");
        HttpEntity<Planet> entity = new HttpEntity<Planet>(planet);

        ResponseEntity<Planet> result = this.restTemplate.exchange(url.toURI(), HttpMethod.PUT, entity, Planet.class);

        assertThat(result.getStatusCode(), is(HttpStatus.OK));
        assertThat(result.getBody().getName(), is(planet.getName()));
        assertThat(result.getBody().getTerrain(), is(planet.getTerrain()));
        assertThat(result.getBody().getApparitions(), is(planet.getApparitions()));
        assertThat(result.getBody().getClimate(), is(planet.getClimate()));
    }

    @Test
    public void deletePlanet() throws Exception {

        createPlanet();

        final URL url = new URL(getBaseUrl() + ID + "/");
        HttpEntity<Planet> entity = new HttpEntity<Planet>(new Planet());

        ResponseEntity<Planet> result = this.restTemplate.exchange(url.toURI(), HttpMethod.DELETE, entity, Planet.class);

        assertThat(result.getStatusCode(), is(HttpStatus.NO_CONTENT));
        assertThat(result.getBody(), is(nullValue()));

        assertThat(getCount(), is(0));
    }

    @Test
    public void allPlanets() throws Exception {
        createPlanet();

        ResponseEntity<String> result = this.restTemplate.getForEntity(getBaseUrl(), String.class);
        assertThat(result.getStatusCode(), is(HttpStatus.OK));
        assertThat(
                result.getBody(),
                is(notNullValue())
        );

        assertThat(getCount(), is(1));
    }

    @Test
    public void findPlanetByName() throws Exception {
        createPlanet();

        final String url = "http://localhost:" + this.port + "/planet/search/findByName?name=Tatooooo";
        assertThat(getCount(url), is(1));
    }

    private String getBaseUrl() {
        return "http://localhost:" + this.port + "/planet/";
    }

    protected int getCount() throws MalformedURLException, URISyntaxException {
        return getCount(getBaseUrl());
    }

    protected int getCount(String url) throws MalformedURLException, URISyntaxException {
        ResponseEntity<Map> result = this.restTemplate.getForEntity(new URL(url).toURI(), Map.class);

        if (result.getStatusCode() != HttpStatus.NOT_FOUND) {
            final Map body = result.getBody();
            if (body == null) {
                return 0;
            }

            final Map embedded = (Map) body.get("_embedded");
            if (embedded == null) {
                return 0;
            }

            final List planet = (List) embedded.get("planet");
            return planet == null ? 0 : planet.size();
        }
        return 0;
    }

}

