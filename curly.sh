#!/bin/bash

TESTE1=$(curl -X POST -H 'Content-Type: application/json' -d '{"id": 556, "name": "Breesil", "terrain": "desert", "climate": "arid"}' http://localhost:8080/planet)
echo "TESTE1"
if [[ -n "$TESTE1" ]]; then
    if [[ "$TESTE1" =~ "Breesil" ]]; then
        echo "SUCESSO"
    else
        echo "FALHA"
        exit
    fi
else
    echo "FALHA"
    exit
fi
TESTE1=$(curl -X POST -H 'Content-Type: application/json' -d '{"id": 556, "name": "Breesil", "terrain": "desert", "climate": "arid"}' http://localhost:8080/planet)
echo "TESTE1"
if [[ -n "$TESTE1" ]]; then
    if [[ "$TESTE1" =~ "Breesil" ]]; then
        echo "SUCESSO"
    else
        echo "FALHA"
        exit
    fi
else
    echo "FALHA"
    exit
fi
TESTE5=$(curl -X GET -H 'Content-Type: application/json' http://localhost:8080/planet/search/findByName?name=Breesil)
echo "TESTE5"
if [[ -n "$TESTE5" ]]; then
    if [[ "$TESTE5" =~ "Breesil" ]]; then
        echo "SUCESSO"
    else
        echo "FALHA"
        exit
    fi
else
    echo "FALHA"
    exit
fi
TESTE2=$(curl -X GET -H 'Content-Type: application/json' http://localhost:8080/planet/556)
echo "TESTE2"
if [[ -n "$TESTE2" ]]; then
    echo "SUCESSO"
else
    echo "FALHA"
    exit
fi
TESTE3=$(curl -X DELETE -H 'Content-Type: application/json' http://localhost:8080/planet/556)
echo "TESTE3"
if [[ -z "$TESTE3" ]]; then
    echo "SUCESSO"
else
    echo "FALHA"
    exit
fi
TESTE4=$(curl -X GET http://localhost:8080/planet/556)
echo "TESTE4"
if [[ -z "$TESTE4" ]]; then
    echo "SUCESSO"
else
    echo "FALHA"
    exit
fi