# B2SWAPI

Para possibilitar a equipe de front criar essa aplicação, queremos desenvolver uma API que contenha os dados dos planetas.
 
## Requisitos

A API deve ser: **REST**
 
Para cada planeta, os seguintes dados devem ser obtidos do banco de dados da aplicação, sendo inserido manualmente:

* Nome
* Clima
* Terreno

Para cada planeta também devemos ter a quantidade de aparições em filmes, que podem ser obtidas pela API pública do Star Wars:  https://swapi.co/

### Funcionalidades desejadas: 

- Adicionar um planeta (com nome, clima e terreno)
- Listar planetas
- Buscar por nome
- Buscar por ID
- Remover planeta

Linguagens que usamos: Java, Go, Clojure, Node, Python
Bancos que usamos: MongoDB, Cassandra, DynamoDB, Datomic

E lembre-se! Um bom software é um software bem testado.

## Projeto

O projeto foi feito utilizando Spring boot, MongoDB e Maven.

### Compilação e execução

No mesmo diretório do projeto, execute:

```
./mvnw clean package
```

Depois execute o serviço:

```
java -jar ./target/b2swapi-1.0-SNAPSHOT.jar
```

ou

```
./mvnw spring-boot:run
```

### Rest API

#### Requisitos

Os testes abaixo foram feito usando cURL (https://curl.haxx.se/), também podem ser encontrados em ```curly.sh``` e em ```PlanetClientTest.java```

##### Listar planetas

```
curl -X GET http://localhost:8080/planet/
```

##### Adicionar um planeta

```
curl -X POST -H "Content-Type: application/json" -d "{"id": 556, "name": "Breesil", "terrain": "desert", "climate": "arid"}' http://localhost:8080/planet
```

##### Buscar por ID

```
curl -X GET -H 'Content-Type: application/json' http://localhost:8080/planet/{ID}
```

##### Buscar por Nome

```
curl -X GET -H 'Content-Type: application/json' http://localhost:8080/planet/search/findByName?name={NOME}
```

##### Remover planeta

```
curl -X DELETE -H 'Content-Type: application/json' http://localhost:8080/planet/{ID}
```